# dragoncurve
Yesterday night I sat there thinking about how easy it would be to
implement the dragoncurve in Haskell, and then how much easier it would
become if one simply defines all iterations of the dragoncurve and that
this would be a nice practice to learn to use
[Diagrams](http://projects.haskell.org/diagrams/). Writing the curve-generation took approximately 30 minutes (I am slow). Creating a diagram was another 2 hours and then I had to find out to use -S instead of -s on the commandline. for diagram selection over the course of two days. Funny world. Yet that's the way it is. Have fun.


# INSTALLATION

I actually just started to use
[stack](https://github.com/commercialhaskell/stack), which means you
might run into trouble. 

But with me a combination of 

* stack build
* stack install 

got me to the point where I could call dragoncurve from some directory

# RUNNING

The main hint is, that you have to select an output file using -o <file> and
one of the iterations using -S <num>. Current output format probably is
SVG. (I have not gotten around playing with the output formats and would
like hints how to do a proper GUI using Diagrams).
