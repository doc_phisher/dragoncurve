{-# LANGUAGE NoMonomorphismRestriction, FlexibleContexts, TypeFamilies #-}

import Diagrams.Prelude
import Diagrams.Backend.SVG.CmdLine
--import Diagrams.Backend.Cairo.CmdLine

import System.Environment

import qualified L as L


dragoncurves:: [[L.Move]]
dragoncurves = iterate dragonnext [L.E]
                where
                  dragonnext :: L.Curve -> L.Curve
                  dragonnext l = l ++ (map (L.rotateleft.L.backwards) (reverse l))
                  

--moveSegment:: L.Move -> Segment
moveSegment L.E = unitX
moveSegment L.W = unitX # rotateBy (-1/2)
moveSegment L.N = unitY
moveSegment L.S = unitY # rotateBy (-1/2)


{- | Usage:
stack exec dragoncurve -- -o /tmp/dragon.svg  -w 4000 -S 4

See:
-- http://projects.haskell.org/diagrams/doc/cmdline.html
| -}
main = mainWith $ zip (map show [1..]) (map genDiagram dragoncurves) 
  where
    genDiagram:: L.Curve -> Diagram B
    genDiagram = fromOffsets.(map moveSegment)
