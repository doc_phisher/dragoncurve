module L where

data Move = E | S | W | N deriving (Show, Read)
type Curve = [Move]

backwards :: Move -> Move
backwards E = W
backwards W = E
backwards N = S
backwards S = N

rotateleft :: Move -> Move
rotateleft E = N
rotateleft W = S
rotateleft N = W
rotateleft S = E

rotateright :: Move -> Move
rotateright = rotateleft.backwards
